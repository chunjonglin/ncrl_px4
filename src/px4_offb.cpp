// ros include
#include <ros/ros.h>
#include <ncrl_px4/px4_control.h>

struct XYZyaw goal;
struct XYZyaw pose;
struct XYZyaw err;
struct XYZyaw err_l;
struct XYZyaw cmd;
struct PID pid_z;
struct PID pid_yaw;
struct PID pid_x;
struct PID pid_y;

double roll = 0;
double pitch = 0;
double yaw = 0;

int flag = 1;
int frequency = 0;

mavros_msgs::State current_state;
geometry_msgs::PoseStamped host_mocap;
geometry_msgs::TwistStamped cmd_vel;

// trajectory msgs
geometry_msgs::Point p_goal;
geometry_msgs::Point v_goal;

// checking msg
geometry_msgs::Point pos_cmd;
geometry_msgs::Point pos_now; 

mavros_msgs::SetMode offb_set_mode;
mavros_msgs::CommandBool arm_cmd;

ros::ServiceClient arming_client;
ros::ServiceClient set_mode_client;

void flag_control()
{
  int c = getch();
  if (c != EOF){
    switch(c){
      case 49:     // key 1
      flag = 1;
      break;
      case 50:     // key 2
      flag = 2;
      break;
      case 51:     // key 3
      flag = 3;
      break;
      case 52:     // key 4
      flag = 4;
      break;
    }
  }
}

void keyboard_control()
{
  int c = getch();
  //ROS_INFO("C: %d",c);
  if (c != EOF) {
    switch (c)
    {
      case 49:     // key 1
      	flag = 1;
      	break;
      case 50:     // key 2
      	flag = 2;
      	break;
      case 51:     // key 3
      	flag = 3;
      	break;
      case 52:     // key 4
      	flag = 4;
      	break;
      case 111:     // key up  o
        goal.z += 0.05;
        break;
      case 112:     // key down p
        goal.z -= 0.05;
        break;
      case 119:     // key foward  w
        goal.x += 0.05;
        break;
      case 115:     // key back   s
        goal.x -= 0.05;
        break;
      case 97:      // key left    a
        goal.y += 0.05;
        break;
      case 100:     // key right   d
        goal.y -= 0.05;
        break;
      case 114:{     // key return  r
        //goal.x = 0;
        //goal.y = 0;
        goal.z = 0;
        //goal.yaw = 0;
        break;
      }
      case 105:    // i
        goal.yaw -= 0.05;
        break;
      case 117:    // u
        goal.yaw += 0.05;
        break;
      case 108:{    // close arming l
        offb_set_mode.request.custom_mode = "MANUAL";
        set_mode_client.call(offb_set_mode);
        arm_cmd.request.value = false;
        arming_client.call(arm_cmd);
        break;
      }
    }
  }
}

void state_cb(const mavros_msgs::State::ConstPtr& msg) {
    current_state = *msg;
}

void host_pos(const geometry_msgs::PoseStamped::ConstPtr& msg){
  host_mocap = *msg;
  tf::Quaternion orientation;
  tf::quaternionMsgToTF(host_mocap.pose.orientation, orientation);
  tf::Matrix3x3(orientation).getRPY(roll, pitch, yaw);

  pose.x = host_mocap.pose.position.x;
  pose.y = host_mocap.pose.position.y;
  pose.z = host_mocap.pose.position.z;
  pos_now.x = host_mocap.pose.position.x;
  pos_now.y = host_mocap.pose.position.y;
  pos_now.z = host_mocap.pose.position.z;
}

// trajectory callback function
void p_goal_cb(const geometry_msgs::Point::ConstPtr& msg){
  p_goal = *msg;
}

void v_goal_cb(const geometry_msgs::Point::ConstPtr& msg){
  v_goal = *msg;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "px4_offb");
  ros::NodeHandle nh;

  // declare subsciber and publisher
  // ros::Subscriber host_sub = nh.subscribe<geometry_msgs::PoseStamped>("/vrpn_client_node/RigidBody1/pose", 10, host_pos);
  ros::Subscriber POS_sub = nh.subscribe<geometry_msgs::PoseStamped>("/px4_POS", 10, host_pos);
  ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State> ("/mavros/state", 10, state_cb);
  ros::Subscriber p_goal_sub = nh.subscribe<geometry_msgs::Point>("/pos", 10, p_goal_cb);
  ros::Subscriber v_goal_sub = nh.subscribe<geometry_msgs::Point>("/vel", 10, v_goal_cb);
  
  ros::Publisher mocap_pos_pub = nh.advertise<geometry_msgs::PoseStamped> ("/mavros/mocap/pose", 10);
  ros::Publisher local_vel_pub = nh.advertise<geometry_msgs::TwistStamped>("/mavros/setpoint_velocity/cmd_vel", 10);
  ros::Publisher pos_cmd_pub = nh.advertise<geometry_msgs::Point>("/px4/pos_cmd", 2);
  ros::Publisher pos_pub = nh.advertise<geometry_msgs::Point>("/px4/pos", 2);
  // declare service
  arming_client = nh.serviceClient<mavros_msgs::CommandBool> ("/mavros/cmd/arming");
  set_mode_client = nh.serviceClient<mavros_msgs::SetMode> ("/mavros/set_mode");

  // init
  cmd_vel.twist.linear.x = 0;
  cmd_vel.twist.linear.y = 0;
  cmd_vel.twist.linear.z = 0;
  cmd_vel.twist.angular.x = 0;
  cmd_vel.twist.angular.y = 0;
  cmd_vel.twist.angular.z = 0;

  ros::param::get("~Z_KP",pid_z.KP);
  ros::param::get("~Z_KI",pid_z.KI);
  ros::param::get("~Z_KD",pid_z.KD);

  ros::param::get("~Y_KP",pid_y.KP);
  ros::param::get("~Y_KI",pid_y.KI);
  ros::param::get("~Y_KD",pid_y.KD);

  ros::param::get("~X_KP",pid_x.KP);
  ros::param::get("~X_KI",pid_x.KI);
  ros::param::get("~X_KD",pid_x.KD);

  ros::param::get("~yaw_KP",pid_yaw.KP);
  ros::param::get("~yaw_KI",pid_yaw.KI);
  ros::param::get("~yaw_KD",pid_yaw.KD);

  goal.x = 0;
  goal.y = 0;
  ros::param::get("~desired_height",goal.z);
  p_goal.x = 0;
  p_goal.y = 0;
  p_goal.z = goal.z;
  ros::param::get("~ctrl_frequency",frequency);

  std::cout << "initial setting" << std::endl;
  std::cout << "z_KP : " << pid_z.KP << " KI : " << pid_z.KI << " KD : " << pid_z.KD << std::endl;
  std::cout << "y_KP : " << pid_y.KP << " KI : " << pid_y.KI << " KD : " << pid_y.KD << std::endl;
  std::cout << "x_KP : " << pid_x.KP << " KI : " << pid_x.KI << " KD : " << pid_x.KD << std::endl;
  std::cout << "yaw_x : " << pid_yaw.KP << " y : " << pid_yaw.KI << " z : " << pid_yaw.KD << std::endl;
  std::cout << "goal_x : " << goal.x << " y : " << goal.y << " z : " << goal.z << std::endl;
  std::cout << "frequency : "<< frequency << std::endl;

  // The setpoint publishing rate MUST be faster than 2Hz.
  ros::AsyncSpinner spinner(2);
  spinner.start();
  ros::Rate rate(frequency);

  // Wait for FCU connection.
  while (ros::ok() && current_state.connected) {
  //mocap_pos_pub.publish(host_mocap);
      ros::spinOnce();
      rate.sleep();
  }

  //send a few setpoints before starting
  for(int i = 100; ros::ok() && i > 0; --i){
    local_vel_pub.publish(cmd_vel);
    //mocap_pos_pub.publish(host_mocap);
    ros::spinOnce();
    rate.sleep();
  }

  offb_set_mode.request.custom_mode = "OFFBOARD";
  arm_cmd.request.value = true;

  ros::Time last_request = ros::Time::now();

  while (ros::ok()) {
    if (current_state.mode != "OFFBOARD" && (ros::Time::now() - last_request > ros::Duration(5.0))) {
      if( set_mode_client.call(offb_set_mode) && offb_set_mode.response.mode_sent) {
          ROS_INFO("Offboard enabled");
      }
      last_request = ros::Time::now();
    }
    else {
      if (!current_state.armed && (ros::Time::now() - last_request > ros::Duration(5.0))) {
        if( arming_client.call(arm_cmd) && arm_cmd.response.success) {
            ROS_INFO("Vehicle armed");
        }
        last_request = ros::Time::now();
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    // start to edit
    keyboard_control();
    printf("flag : %d\n",flag);

    if (flag == 2){ // trajectory 
      ROS_INFO("Trajectory\n");
      goal.x = p_goal.x;
      goal.y = p_goal.y;
    }
    else if (flag == 3){ // landing
      ROS_INFO("LANDING \n");
      goal.x = pose.x;
      goal.y = pose.y;
      goal.z = 0;
    }
    else if (flag == 4){ // lock
      ROS_INFO("LOCK \n");
      offb_set_mode.request.custom_mode = "MANUAL";
      set_mode_client.call(offb_set_mode);
      arm_cmd.request.value = false;
      arming_client.call(arm_cmd);
    }
    else{
      ROS_INFO("keyboard control\n");
    }

    pos_cmd.x = goal.x;
    pos_cmd.y = goal.y;
    pos_cmd.z = goal.z;

    if (goal.yaw > M_PI || goal.yaw < -M_PI){
      if (goal.yaw > M_PI)
        goal.yaw -= 2*M_PI;
      else
        goal.yaw += 2*M_PI;
    }

    err.x = goal.x - pose.x;
    err.y = goal.y - pose.y;
    err.z = goal.z - pose.z;
    err.yaw = goal.yaw - yaw;

    if (err.yaw > M_PI || err.yaw < -M_PI){
      if (err.yaw > M_PI)
        err.yaw -= 2*M_PI;
      else
        err.yaw += 2*M_PI;
    }

    printf("pose : %f \t %f \t %f \t %f\n",pose.x, pose.y, pose.z, RAD2DEG(yaw));
    // printf("goal : %f \t %f \t %f \t %f\n",goal.x, goal.y, goal.z, RAD2DEG(goal.yaw));
    // printf("err : %f \t %f \t %f \n",err.x, err.y, err.z);

    pid_compute(pid_x, cmd.x, err.x, err_l.x, 0.5);
    pid_compute(pid_y, cmd.y, err.y, err_l.y, 0.5);
    pid_compute(pid_z, cmd.z, err.z, err_l.z, 0.5);
    pid_compute(pid_yaw, cmd.yaw, err.yaw, err_l.yaw, 0.5);

    if (flag == 2){ // trajectory with feed forward term
      cmd_vel.twist.linear.x = cmd.x + v_goal.x;
      cmd_vel.twist.linear.y = cmd.y + v_goal.y;
      cmd_vel.twist.linear.z = cmd.z;
      cmd_vel.twist.angular.z = cmd.yaw;
    }else{
      cmd_vel.twist.linear.x = cmd.x;
      cmd_vel.twist.linear.y = cmd.y;
      cmd_vel.twist.linear.z = cmd.z;
      cmd_vel.twist.angular.z = cmd.yaw;
    }

    local_vel_pub.publish(cmd_vel);
    err_l = err;
    pos_cmd_pub.publish(pos_cmd);
    pos_pub.publish(pos_now);
    ////////////////////////////////////////////////////////////////////////////
    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}
