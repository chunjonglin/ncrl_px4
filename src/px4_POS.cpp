/*
 * POS(position and orientation system
 */
#include <ros/ros.h>
#include <ncrl_px4/px4_control.h>
#include <ncrl_px4/ncrl_tf.h>

// for check
//ros::Publisher pub_loam_pos;
//ros::Publisher pub_loam_rpy;
//ros::Publisher pub_mocap_pos;
//ros::Publisher pub_mocap_rpy;
//ros::Publisher pub_vio_pos;
//ros::Publisher pub_vio_rpy;
// visual 
//ros::Publisher marker_pub1;
//ros::Publisher marker_pub2;
//ros::Publisher marker_pub3;
// final
ros::Publisher pub_POS;
//ros::Publisher pub_POS_test;
ros::Publisher pub_POS_status;

//visualization_msgs::Marker points1;
//visualization_msgs::Marker points2;
//visualization_msgs::Marker points3;

nav_msgs::Odometry aloam_msg_input;
nav_msgs::Odometry lio_msg_input;

// output
geometry_msgs::PoseStamped POS_msg;

// candidate
geometry_msgs::PoseStamped mocap_msg;
geometry_msgs::PoseStamped aloam_msg;
geometry_msgs::PoseStamped lio_msg;


//geometry_msgs::PoseStamped POS_msg_test;

// pos msg
//geometry_msgs::Point pos_loam;
//geometry_msgs::Point pos_vio;
//geometry_msgs::Point pos_mocap;

// euler msg
//geometry_msgs::Point rpy_loam;
//geometry_msgs::Point rpy_vio;
//geometry_msgs::Point rpy_mocap;

// bias msg
geometry_msgs::Point pos_aloam_bias;
geometry_msgs::Point rpy_aloam_bias;
geometry_msgs::Point pos_mocap_bias;
geometry_msgs::Point rpy_mocap_bias;
geometry_msgs::Point pos_lio_bias;
geometry_msgs::Point rpy_lio_bias;
ncrl_tf::Trans aloam_bias;
ncrl_tf::Trans lio_bias;

// switch
geometry_msgs::PointStamped POS_status;

//double roll_loam, pitch_loam, yaw_loam;
//double roll_vio, pitch_vio, yaw_vio;
//double roll_mocap, pitch_mocap, yaw_mocap;

// time setting
double t_start, t_current;

int aloam_bias_count = 0;
int mocap_bias_count = 0;
int lio_bias_count = 0;

int flag = 1;

bool aloam_msg_in = false;
bool mocap_msg_in = false;
bool lio_msg_in = false;

//void transformToCentroid(geometry_msgs::PoseStamped& pose_msg, double tx, double ty, double tz){
//  geometry_msgs::PoseStamped pos_in = pose_msg;
//  double rx, ry, rz;

//  tf::Quaternion orientation;
//  tf::quaternionMsgToTF(pos_in.pose.orientation, orientation);
//  tf::Matrix3x3(orientation).getRPY(rx, ry, rz);
//  // from bodyframe to inertia frame
//  rotation_3D(tx, ty, tz, rx, ry, rz);
//  pos_in.pose.position.x += tx;
//  pos_in.pose.position.y += ty;
//  pos_in.pose.position.z += tz;
//  pose_msg = pos_in;
//}

//void visualization_setting(visualization_msgs::Marker& points, geometry_msgs::Point pos, int color){
//  // visualization marker setting
//  points.header.frame_id = "/init";
//  points.ns = "points";
//  points.action = visualization_msgs::Marker::ADD;
//  points.pose.orientation.w = 1.0;
//  points.id = 0;
//  points.type = visualization_msgs::Marker::POINTS;
//  // POINTS markers use x and y scale for width/height respectively
//  points.scale.x = 0.05;
//  points.scale.y = 0.05;
//  // Points are red
//  if (color == 1){
//    points.color.r = 1.0f;
//    points.color.a = 1.0;
//  } else if (color == 2){
//    points.color.g = 1.0f;
//    points.color.a = 1.0;
//  } else if (color == 3){
//    points.color.b = 1.0f;
//    points.color.a = 1.0;
//  }

//  points.points.push_back(pos);

//  if (color == 1){
////    marker_pub1.publish(points);
//  } else if (color == 2){
////    marker_pub2.publish(points);
//  }
//}

void aloam_cb(const nav_msgs::Odometry::ConstPtr& msg){
  aloam_msg_input = *msg;
  aloam_msg.header = aloam_msg_input.header;
  aloam_msg.pose = aloam_msg_input.pose.pose;
  aloam_msg_in = true;
//  aloam_msg.header = aloam_msg_input.header;
  //	tf::Quaternion orientation;
  //	tf::quaternionMsgToTF(lidar_msg.pose.pose.orientation, orientation);
  //	tf::Matrix3x3(orientation).getRPY(roll_loam, pitch_loam, yaw_loam);
  //  pos_loam = lidar_msg.pose.pose.position;

//  double rx1, ry1, rz1;
//  double rx2, ry2, rz2;
//  double rx3, ry3, rz3;
//  // init to camera_init
//  rx1 = M_PI/2;
//  ry1 = 0;
//  rz1 = M_PI/2;

//  // camera to body
//  rx2 = 0;
//  ry2 = -M_PI/2;
//  rz2 = -M_PI/2;

//  double x, y, z;
//  x = pos_loam.x;
//  y = pos_loam.y;
//  z = pos_loam.z;

//  // trans odometry(camera_init -> camera) to (init -> body)
//  accumulate_Rotation(rx1,       ry1,        rz1,
//                      roll_loam, pitch_loam, yaw_loam,
//                      rx3,       ry3,        rz3);
//  accumulate_Rotation(rx3, ry3, rz3,
//                      rx2, ry2, rz2,
//                      rx3, ry3, rz3);
//  rotation_3D(x, y, z, -rx2, -ry2, -rz2);

  // update loam_msg and transform to centroid
//  loam_msg.pose.position.x = x;
//  loam_msg.pose.position.y = y;
//  loam_msg.pose.position.z = z;
//  tf::Quaternion quat;
//  quat.setRPY(rx3, ry3, rz3);
//  tf::quaternionTFToMsg(quat, loam_msg.pose.orientation);
////  transformToCentroid(loam_msg, 0, 0, -0.10);

//  pos_loam.x = loam_msg.pose.position.x;
//  pos_loam.y = loam_msg.pose.position.y;
//  pos_loam.z = loam_msg.pose.position.z;

//  rpy_loam.x = RAD2DEG(rx3);
//  rpy_loam.y = RAD2DEG(ry3);
//  rpy_loam.z = RAD2DEG(rz3);

  // caculate the bias
//  if(!lidar_msg_in &&
//     aloam_msg_input.pose.pose.position.x != 0 &&
//     aloam_msg_input.pose.pose.position.y != 0 &&
//     aloam_msg_input.pose.pose.position.z != 0){

//     lidar_msg_in = true;
////    loam_bias_count += 1;
////    pos_aloam_bias.x += aloam_msg_input.pose.pose.position.x;
////    pos_aloam_bias.y += aloam_msg_input.pose.pose.position.y;
////    pos_aloam_bias.z += aloam_msg_input.pose.pose.position.z;
////    Eigen::Quaterniond tmp_q(aloam_msg_input.pose.pose.orientation.w,
////                             aloam_msg_input.pose.pose.orientation.x,
////                             aloam_msg_input.pose.pose.orientation.y,
////                             aloam_msg_input.pose.pose.orientation.z);
////    Eigen::Vector3d tmp_v = ncrl_tf::Q2Euler(tmp_q);

////    rpy_aloam_bias.x += tmp_v.x();
////    rpy_aloam_bias.y += tmp_v.y();
////    rpy_aloam_bias.z += tmp_v.z();
////    if(loam_bias_count == 10){
////      lidar_msg_in = true;
////      pos_aloam_bias.x /= 10;
////      pos_aloam_bias.y /= 10;
////      pos_aloam_bias.z /= 10;
////      rpy_aloam_bias.x /= 10;
////      rpy_aloam_bias.y /= 10;
////      rpy_aloam_bias.z /= 10;
////      Eigen::Vector3d v_(pos_aloam_bias.x, pos_aloam_bias.y, pos_aloam_bias.z);
////      Eigen::Vector3d e_(rpy_aloam_bias.x, rpy_aloam_bias.y, rpy_aloam_bias.z);
////      Eigen::Quaterniond q_ = ncrl_tf::Euler2Q(e_);
////      ncrl_tf::setTransFrame(aloam_bias, "INIT", "ALOAM_INIT");
////      ncrl_tf::setTrans(aloam_bias, q_, v_);
////    }
//  }

  // minus the bias of position
//  if (lidar_msg_in){
//    ncrl_tf::setTransFrame(aloam_trans, "", "")
//    pos_loam.x -= pos_loam_bias.x;
//    pos_loam.y -= pos_loam_bias.y;
//    pos_loam.z -= pos_loam_bias.z;

//    pub_loam_pos.publish(pos_loam);
//    pub_loam_rpy.publish(rpy_loam);

//    loam_msg.pose.position = pos_loam;
//  }

  // visualize
//  visualization_setting(points1, pos_loam, 1);
}

void lio_cb(const nav_msgs::Odometry::ConstPtr& msg){
  lio_msg_input = *msg;
  lio_msg_in = true;
  lio_msg.header = lio_msg_input.header;
  lio_msg.pose = lio_msg_input.pose.pose;
//  tf::Quaternion orientation;
//  tf::quaternionMsgToTF(vio_msg.pose.pose.orientation, orientation);
//  tf::Matrix3x3(orientation).getRPY(roll_vio, pitch_vio, yaw_vio);

//  vins_msg.header = vio_msg.header;Wdfdf
//  // transform to centroid
//  vins_msg.pose.position.x = vio_msg.pose.pose.position.x;
//  vins_msg.pose.position.y = vio_msg.pose.pose.position.y;
//  vins_msg.pose.position.z = vio_msg.pose.pose.position.z;
//  vins_msg.pose.orientation = vio_msg.pose.pose.orientation;

//  transformToCentroid(vins_msg, -0.15, 0, 0.05);

//  pos_vio = vins_msg.pose.position;

//  rpy_vio.x = RAD2DEG(roll_vio);
//  rpy_vio.y = RAD2DEG(pitch_vio);
//  rpy_vio.z = RAD2DEG(yaw_vio);

  // caculate the bias
//  if (!vio_msg_in &&
//     pos_vio.x != 0 && pos_vio.y != 0 && pos_vio.z != 0 &&
//     rpy_vio.x != 0 && rpy_vio.y != 0 && rpy_vio.z != 0){

//    vio_bias_count += 1;
//    pos_vio_bias.x += pos_vio.x;
//    pos_vio_bias.y += pos_vio.y;
//    pos_vio_bias.z += pos_vio.z;
//    rpy_vio_bias.x += rpy_vio.x;
//    rpy_vio_bias.y += rpy_vio.y;
//    rpy_vio_bias.z += rpy_vio.z;
//    if (vio_bias_count == 10){
//      vio_msg_in = true;
//      pos_vio_bias.x /= 10;
//      pos_vio_bias.y /= 10;
//      pos_vio_bias.z /= 10;
//      rpy_vio_bias.x /= 10;
//      rpy_vio_bias.y /= 10;
//      rpy_vio_bias.z /= 10;
//    }
//  }

  // minus the bias of position
//  if(vio_msg_in){
//    pos_vio.x -= pos_vio_bias.x;
//    pos_vio.y -= pos_vio_bias.y;
//    pos_vio.z -= pos_vio_bias.z;
//    vins_msg.pose.position = pos_vio;
////    pub_vio_pos.publish(pos_vio);
////    pub_vio_rpy.publish(rpy_vio);
//  }

//  visualization_setting(points2, pos_vio, 2);
}

void mocap_cb(const geometry_msgs::PoseStamped::ConstPtr& msg){
  mocap_msg = *msg;

  if (!mocap_msg_in && mocap_msg.pose.position.x != 0){
      mocap_bias_count++;
      pos_mocap_bias.x += mocap_msg.pose.position.x;
      pos_mocap_bias.y += mocap_msg.pose.position.y;
      pos_mocap_bias.z += mocap_msg.pose.position.z;
      if (mocap_bias_count >= 10){
          mocap_msg_in = true;
          pos_mocap_bias.x /= 10;
          pos_mocap_bias.y /= 10;
          pos_mocap_bias.z /= 10;
      }
  } else {
//      mocap_msg.pose.position.x -= pos_mocap_bias.x;
//      mocap_msg.pose.position.y -= pos_mocap_bias.y;
      mocap_msg.pose.position.z -= pos_mocap_bias.z;
  }
//  tf::Quaternion orientation;
//  tf::quaternionMsgToTF(mocap_msg.pose.orientation, orientation);
//  tf::Matrix3x3(orientation).getRPY(roll_mocap, pitch_mocap, yaw_mocap);
//  pos_mocap = mocap_msg.pose.position;
//  rpy_mocap.x = RAD2DEG(roll_mocap);
//  rpy_mocap.y = RAD2DEG(pitch_mocap);
//  rpy_mocap.z = RAD2DEG(yaw_mocap);

//  if(!mocap_msg_in &&
//     pos_mocap.x != 0 && pos_mocap.y != 0 && pos_mocap.z != 0 &&
//     rpy_mocap.x != 0 && rpy_mocap.y != 0 && rpy_mocap.z != 0){
//    mocap_bias_count += 1;
//    pos_mocap_bias.x += pos_mocap.x;
//    pos_mocap_bias.y += pos_mocap.y;
//    pos_mocap_bias.z += pos_mocap.z;
//    rpy_mocap_bias.x += rpy_mocap.x;
//    rpy_mocap_bias.y += rpy_mocap.y;
//    rpy_mocap_bias.z += rpy_mocap.z;
//    if(mocap_bias_count == 10){
//      mocap_msg_in = true;
//      pos_mocap_bias.x /= 10;
//      pos_mocap_bias.y /= 10;
//      pos_mocap_bias.z /= 10;
//      rpy_mocap_bias.x /= 10;
//      rpy_mocap_bias.y /= 10;
//      rpy_mocap_bias.z /= 10;
//    }
//  }

//  if (mocap_msg_in){
//    pos_mocap.x -= pos_mocap_bias.x;
//    pos_mocap.y -= pos_mocap_bias.y;
//    pos_mocap.z -= pos_mocap_bias.z;
//    mocap_msg.pose.position = pos_mocap;
////    transformToCentroid(mocap_msg, 0, 0, -0.15);
////    pub_mocap_pos.publish(pos_mocap);
////    pub_mocap_rpy.publish(rpy_mocap);
//  }
}

//void broadcastTF(const ros::TimerEvent& timer_event){
//  static tf::TransformBroadcaster br;
//  tf::Transform tf_map_lidar;
//  tf::Quaternion q_map_lidar;
//  tf::Vector3 v_map_lidar;

//  v_map_lidar.setValue(pos_loam.x, pos_loam.y, pos_loam.z);
//  q_map_lidar.setRPY(DEG2RAD(rpy_loam.x), DEG2RAD(rpy_loam.y), DEG2RAD(rpy_loam.z));
//  tf_map_lidar.setOrigin(v_map_lidar);
//  tf_map_lidar.setRotation(q_map_lidar);

//  br.sendTransform(tf::StampedTransform(tf_map_lidar, ros::Time::now(), "init", "loam"));

//  tf::Transform tf_map_vio;
//  tf::Quaternion q_map_vio;
//  tf::Vector3 v_map_vio;

//  v_map_vio.setValue(pos_vio.x, pos_vio.y, pos_vio.z);
//  q_map_vio.setRPY(DEG2RAD(rpy_vio.x), DEG2RAD(rpy_vio.y), DEG2RAD(rpy_vio.z));
//  tf_map_vio.setOrigin(v_map_vio);
//  tf_map_vio.setRotation(q_map_vio);

//  br.sendTransform(tf::StampedTransform(tf_map_vio, ros::Time::now(), "init", "vio"));
//}

void keyboard_control()
{
  int c = getch();
  //ROS_INFO("C: %d",c);
  if (c != EOF) {
    switch (c)
    {
      case 49:     // key 1
      	flag = 1;
      	break;
      case 50:     // key 2
      	flag = 2;
      	break;
      case 51:     // key 3
      	flag = 3;
      	break;
      case 52:     // key 4
      	flag = 4;
      	break;
    }
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "px4_POS");
  ros::NodeHandle nh;

  ros::Subscriber sub_loam = nh.subscribe<nav_msgs::Odometry> ("/aft_mapped_to_init",2 ,&aloam_cb);
  ros::Subscriber sub_lio = nh.subscribe<nav_msgs::Odometry> ("/estimator/imu_propagate_est",2, &lio_cb);
  ros::Subscriber sub_host_mocap = nh.subscribe<geometry_msgs::PoseStamped> ("vrpn_client_node/RigidBody7/pose",2 , &mocap_cb);
  
//  pub_loam_pos = nh.advertise<geometry_msgs::Point> ("loam/pos", 2);
//  pub_loam_rpy = nh.advertise<geometry_msgs::Point> ("loam/rpy", 2);
//  pub_mocap_pos = nh.advertise<geometry_msgs::Point> ("mocap/pos", 2);
//  pub_mocap_rpy = nh.advertise<geometry_msgs::Point> ("mocap/rpy", 2);
//  pub_vio_pos = nh.advertise<geometry_msgs::Point> ("vio/pos",2);
//  pub_vio_rpy = nh.advertise<geometry_msgs::Point> ("vio/rpy",2);
//  marker_pub1 = nh.advertise<visualization_msgs::Marker>("/visualization_marker1", 10);
//  marker_pub2 = nh.advertise<visualization_msgs::Marker>("/visualization_marker2", 10);
//  marker_pub3 = nh.advertise<visualization_msgs::Marker>("/visualization_marker3", 10);
  pub_POS = nh.advertise<geometry_msgs::PoseStamped> ("/px4_POS", 2);
//  pub_POS_test = nh.advertise<geometry_msgs::PoseStamped> ("px4_POS_test", 2);
  pub_POS_status = nh.advertise<geometry_msgs::PointStamped> ("px4_POS_status", 2);

  t_start = ros::Time::now().toSec();
//  ros::Timer timer = nh.createTimer(ros::Duration(0.5), broadcastTF);
  ros::Rate r(100);
  while(ros::ok()){
//    if (flag == 2){
//      // 5 sec mocap & 5 sec lidar
//      t_current = ros::Time::now().toSec();
//      if (t_current >= (t_start + 2)){
//        if (POS_status.z == 1){
//          POS_status.x = 0;
//          POS_status.y = 1;
//          POS_status.z = 0;
//        } else {
//          POS_status.x = 0;
//          POS_status.y = 0;
//          POS_status.z = 1;
//        }
//        t_start = t_current;
//      }

//      if (POS_status.z == 1){
//        POS_msg_test = loam_msg;
//      } else {
//        POS_msg_test = mocap_msg;
//      }
//    }

    keyboard_control();
    if (flag == 1){
      ROS_INFO("FLAG 1 : ONLY USE MOCAP");
      POS_status.point.x = 0;
      POS_status.point.y = 0;
      POS_status.point.z = 1;
      POS_msg = mocap_msg;
      POS_msg.header.frame_id = "init";
      pub_POS.publish(POS_msg);
    }

    else if (flag == 2){
      ROS_INFO("FLAG 2 : LIO");
      POS_status.point.x = 1;
      POS_status.point.y = 0;
      POS_status.point.z = 0;
      POS_msg = lio_msg;
      POS_msg.header.frame_id = "init";
      pub_POS.publish(POS_msg);
    }

    else if (flag == 3){
      ROS_INFO("FLAG 3 : ONLY USE LIDAR");
      POS_status.point.x = 0;
      POS_status.point.y = 1;
      POS_status.point.z = 0;
      POS_msg = aloam_msg;
      POS_msg.header.frame_id = "init";
      pub_POS.publish(POS_msg);
    }

//    else if (flag == 4){
//      ROS_INFO("FLAG 4 : ONLY USE VINS");
//      POS_msg = vins_msg;
//      POS_status.x = 1;
//      POS_status.y = 0;
//      POS_status.z = 0;
//      POS_msg.header.frame_id = "init";
//      pub_POS.publish(POS_msg);
//    }

//    pub_POS_test.publish(POS_msg_test);
    POS_status.header = POS_msg.header;
    pub_POS_status.publish(POS_status);

    std::cout << "lio pos   : " << lio_msg.pose.position.x << " " << lio_msg.pose.position.y << " " << lio_msg.pose.position.z << std::endl;
    std::cout << "aloam pos  : " << aloam_msg.pose.position.x << " " << aloam_msg.pose.position.y << " " << aloam_msg.pose.position.z << std::endl;
    std::cout << "mocap pos : " << mocap_msg.pose.position.x << " " << mocap_msg.pose.position.y << " " << mocap_msg.pose.position.z << std::endl << std::endl;

  	ros::spinOnce();
    r.sleep();
  }
}
