# ncrl_px4
* Author : Chun-Jung Lin	chadlin.gdr07g@nctu.edu.tw

## Target :
* controlling UAV with current pose and desired pose

## Node : 
1. px4_POS
	* the node would receive multiple odometries, like SLAM, OptiTrack, LOAM, and publishes an odometry to **px4_offb** as the pose feedback.
2. px3_offb
	* the node receives the odometry from **px4_POS** and the desired motion from the motion planning, and transfer the velocity command to the low-level controller in PX4.

