#ifndef PX4_CONTROL_H
#define PX4_CONTROL_H
#include <cmath>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/Marker.h>
// PX4 include
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
// c++ include
#include <cstdio>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#define GRAVITY 9.81
#define DEG2RAD(DEG) ((DEG) * ((M_PI) / (180.0)))
#define RAD2DEG(RAD) ((RAD) * (180.0) / (M_PI))

using namespace std;
struct XYZyaw{
  float x; float y; float z; float yaw;
};

struct PID{
  float KP; float KI; float KD;
  float PR; float IN; float DE;
};

struct trajectory{
  float px; float vx; float ax;
  float py; float vy; float ay;
};

char getch(){
    int flags = fcntl(0, F_GETFL, 0);
    fcntl(0, F_SETFL, flags | O_NONBLOCK);

    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0) {
        perror("tcsetattr()");
    }
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0) {
        perror("tcsetattr ICANON");
    }
    if (read(0, &buf, 1) < 0) {
        //perror ("read()");
    }
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0) {
        perror ("tcsetattr ~ICANON");
    }
    return (buf);
}

void confine(float &data, float threshold)
{
  if(data >= threshold || data <= -threshold){
    if(data >= threshold)
      data = threshold;
    else
      data = -threshold;
  }
  else{
    data = data;
  }
}

void rotation_3D (double &X, double &Y, double &Z,double rx,double ry,double rz){
  // rotate with R(z)R(y)R(x)
  // if rotate with frame input -rx -ry -rz;
  float x1 = cos(rz) * X - sin(rz) * Y;
  float y1 = sin(rz) * X + cos(rz) * Y;
  float z1 = Z;

  float x2 = sin(ry) * z1 + cos(ry) * x1;
  float y2 = y1;
  float z2 = cos(ry) * z1 - sin(ry) * x1;

  X = x2;
  Y = cos(rx) * y2 - sin(rx) * z2;
  Z = sin(rx) * y2 + cos(rx) * z2;
}

void accumulate_Rotation (double rx1, double ry1, double rz1,
                          double rx2, double ry2, double rz2,
                          double &rx, double &ry, double &rz){
  tf::Quaternion q_1;
  tf::Quaternion q_2;
  tf::Quaternion q_o;
  q_1.setRPY(rx1, ry1, rz1);
  q_2.setRPY(rx2, ry2, rz2);
  q_o = q_1 * q_2;
  tf::Matrix3x3 mat(q_o);
  double roll, pitch, yaw;
  mat.getRPY(roll, pitch, yaw);
  rx = roll;
  ry = pitch;
  rz = yaw;
}

void rotZ(float &x,float &y, double rz)
{
  float x1 = x;
  float y1 = y;
  x = cos(rz) * x1 - sin(rz) * y1;
  y = sin(rz) * x1 + cos(rz) * y1;
}

void pid_compute(struct PID &pid, float &r, float &e, float &e_l, float c){
  pid.PR = e;
  pid.IN += e;
  pid.DE = e - e_l;
  confine(pid.IN, c);
  r = (pid.PR * pid.KP + pid.IN * pid.KI + pid.DE * pid.KD);
}

void timeDuration(double &start, double &end, double &dur)
{
  start = ros::Time::now().toSec();
  dur = start - end;
  end = start;
}
#endif // PX4_CONTROL_H
